module Refinery
  module Teams
    class Team < Refinery::Core::BaseModel
      self.table_name = 'refinery_teams'

      attr_accessible :name, :function, :description, :image_id, :school, :position

      acts_as_indexed :fields => [:name, :function, :description]

      validates :name, :presence => true, :uniqueness => true

      belongs_to :image, :class_name => '::Refinery::Image'
    end
  end
end
